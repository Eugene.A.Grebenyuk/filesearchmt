﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace FileSearchMT
{
    /// <remarks>
    /// Работа в 1 поток (для сравнения) по SSD диску C:
    /// SearchedDirsCount: 86140
    /// DeniedDirsCount: 391
    /// SearchTime: 49 sec
    /// 
    /// TotalCmd в 1 поток ищет за 32 сек. Проводник Windows - 55 сек в 1 поток.
    /// 
    /// Многопоточный поиск: 16 сек в Debug-режиме! Это на 6 физических и 12 логических ядрах. В 3 раза быстрее. К диску обращений почти нет - всё из кэша в памяти берётся.
    /// При внешнем запуске exe (хоть Build, хоть Debug версии) - 11 сек!
    /// Снял контроль количества потоков - 9 сек!
    /// </remarks>
    /// <author>Eugene A. Grebenyuk</author>
    class MTFileSearch : ISearchProgressTracking
    {
        private int cpuLogicalCoresCount;
        private List<Task> tasks = new List<Task>();
        public event EventHandler NeedToPrintCancelPressed;
        private CancellationTokenSource cts = new CancellationTokenSource();  //Disposable!   //Про отмену: https://docs.microsoft.com/ru-ru/dotnet/standard/threading/cancellation-in-managed-threads
        //private System.Collections.Concurrent.ConcurrentBag<Task> tasks = new System.Collections.Concurrent.ConcurrentBag<Task>();
        public bool isCancel = false;
        private long searchedDirsCount = 0;
        public int deniedDirsCount = 0;
        public int foundFilesCount = 0;
        private long tasksCount = 0;
        private DateTime timeBegin;
        public TimeSpan searchTime;
        private object lockFoundFilesCount = new object();
        private object lockTasksList = new object();

        public long TasksCount { get => Interlocked.Read(ref tasksCount); }
        public long SearchedDirsCount { get => Interlocked.Read(ref searchedDirsCount); }

        public MTFileSearch()
        {
            cpuLogicalCoresCount = Environment.ProcessorCount;
        }

        public void CancelPressed(object sender, ConsoleCancelEventArgs e)
        {
            isCancel = true;
            //cts.Cancel();     //В принципе, и без прерывания потоков довольно быстро отменяется.
            e.Cancel = true;
            NeedToPrintCancelPressed.Invoke(null, new EventArgs());
        }

        public void GoSearch(string searchDisk, string searchPattern)
        {
            timeBegin = DateTime.Now;

            Task t = new Task((object name) => Search(searchDisk, searchPattern),
                string.Format("Main search thread for {0}", searchDisk),
                cts.Token, TaskCreationOptions.AttachedToParent);

            t.Start();
            t.Wait();   //or t.RunSynchronously()
            cts.Dispose();
            searchTime = DateTime.Now - timeBegin;
        }

        private void RemoveCompletedTasksFromList()
        {
            lock (lockTasksList)
            {
                for (int i = 0; i < tasks.Count; i++)
                    if (tasks[i].IsCompleted || tasks[i].Status == TaskStatus.WaitingForChildrenToComplete)
                    {
                        tasks.RemoveAt(i);
                        i--;
                    }
            }
        }

        /// <summary>
        /// Multithreaded
        /// </summary>
        private void Search(string searchDisk, string searchPattern)
        {
            if (isCancel)
                return;

            Interlocked.Increment(ref tasksCount);

            string[] filenames = Directory.GetFiles(searchDisk, searchPattern, SearchOption.TopDirectoryOnly);

            if (filenames.Length > 0)
            {
                lock (lockFoundFilesCount)
                    foundFilesCount += filenames.Length;        //Можно заменить на System.Threading.Interlocked.Add(ref foundFilesCount, filenames.Length), но смысла особого нет.

                foreach (string filename in filenames)
                    Console.WriteLine("Found: {0}", filename);
            }

            Interlocked.Increment(ref searchedDirsCount);  //Быстрее, чем lock(lock1) и searchedDirsCount++, но здесь не критично.

            string[] subdirs = Directory.GetDirectories(searchDisk, "*", SearchOption.TopDirectoryOnly);
            foreach (string subdir in subdirs)
            {
                if (CheckDirAccess(subdir) && !isCancel)
                    SearchThread(subdir, searchPattern);
            }

            //if (tasksCount > maxTasks)
            //    Interlocked.Exchange(ref maxTasks, (int) tasksCount);

            Interlocked.Decrement(ref tasksCount);
        }

        private void SearchThread(string searchDisk, string searchPattern)
        {
#if (CORESCONTROL)
            RemoveCompletedTasksFromList();
            if(tasks.Count < cpuLogicalCoresCount - 1)
            {
#endif
            //Если не контролировать кол-во тасков (создавать их десятками тысяч) - так даже быстрее работать будет (14s vs 16s), и загрузка CPU плотнее.
            Task t = new Task((object name) => Search(searchDisk, searchPattern),
                string.Format("Search thread for {0}", searchDisk),
                cts.Token, TaskCreationOptions.AttachedToParent);
#if (CORESCONTROL)
            lock (lockTasksList)
                tasks.Add(t);
#endif
            t.Start();
#if (CORESCONTROL)
            }
            else
            {
                Search(searchDisk, searchPattern);
            }
#endif
        }

        private bool CheckDirAccess(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);

            if (di.Attributes.HasFlag(FileAttributes.ReparsePoint))
                return false;                                       //Это обычно Mounted Volume из .vhdx, либо линк на другую директорию.

            try
            {
                DirectoryInfo[] dis = di.GetDirectories("*", SearchOption.TopDirectoryOnly);
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                Interlocked.Increment(ref deniedDirsCount);        //Быстрее, чем lock(lock2) и deniedDirsCount++, но здесь не критично.
                return false;
            }
            //catch(PathTooLongException)                           //На Framework 4.6.1 бывает, когда путь длиннее 248 символов. На Framework 4.6.2 проблема решена.
        }
    }
}

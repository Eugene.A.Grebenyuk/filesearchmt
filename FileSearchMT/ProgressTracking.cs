﻿using System;

namespace FileSearchMT
{
    class ProgressTracking
    {
        private static ISearchProgressTracking mtfs;
        internal static ISearchProgressTracking FileSearchInstance { set => mtfs = value; }
        public static event EventHandler<SetConsoleTitleEventArgs> NeedToChangeTittle;

        public static void CheckProgress(object sender)
        {
            if(mtfs != null)
                NeedToChangeTittle.Invoke(null, new SetConsoleTitleEventArgs(string.Format("SearchedDirs: {0} Tasks: {1}", mtfs.SearchedDirsCount, mtfs.TasksCount)));
        }
    }

    public class SetConsoleTitleEventArgs : EventArgs
    {
        public string Title { get; set; }

        public SetConsoleTitleEventArgs(string title)
        {
            Title = title;
        }
    }

    interface ISearchProgressTracking
    {
        long SearchedDirsCount { get; }
        long TasksCount { get; }
    }
}

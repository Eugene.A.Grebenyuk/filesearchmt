﻿#undef CORESCONTROL

using System;
using System.Threading;

namespace FileSearchMT
{
    class Program
    {
        static void Main(string[] args)
        {
            string searchPattern = "ccmexec.exe";
            string searchDisk = "C:\\";

            Console.WriteLine("To search a file by search pattern, type:");

            searchDisk = inputDriveAndCheck();

            Console.WriteLine("Search filename pattern: ");
            searchPattern = Console.ReadLine();
            if (searchPattern.Length < 1)
                return;

            Console.WriteLine("Search for file {0} in {1} started...{2}", searchPattern, searchDisk, Environment.NewLine);

            ProgressTracking.NeedToChangeTittle += SetConsoleTitle;
            Timer progressCheck = new Timer(ProgressTracking.CheckProgress, null, 2000, 2000);

            MTFileSearch mtfs = new MTFileSearch();
            ProgressTracking.FileSearchInstance = mtfs;
            Console.CancelKeyPress += mtfs.CancelPressed;
            mtfs.NeedToPrintCancelPressed += PrintCancelPressed;
            mtfs.GoSearch(searchDisk, searchPattern);
            progressCheck.Dispose();

            Console.WriteLine("SearchedDirsCount: {0}", mtfs.SearchedDirsCount);
            Console.WriteLine("DeniedDirsCount: {0}", mtfs.deniedDirsCount);
            Console.WriteLine("FoundFilesCount: {0}", mtfs.foundFilesCount);
            //Console.WriteLine("MaxTasks: {0}", mtfs.maxTasks);
            Console.WriteLine("SearchTime: {0} sec", Math.Round(mtfs.searchTime.TotalSeconds, 0));

            if (!mtfs.isCancel)
                Console.WriteLine("{0}Done. Press any key.", Environment.NewLine);
            else
                Console.WriteLine("{0}Cancelled. Press any key.", Environment.NewLine);

            Console.ReadKey();
        }

        private static string inputDriveAndCheck()
        {
            string searchDisk;

            do
            {
                do
                {
                    Console.WriteLine("Search disk: ");
                    searchDisk = Console.ReadLine();
                }
                while (searchDisk.Length < 1 || searchDisk.Length > 3);
                searchDisk = searchDisk.Contains(":") ? searchDisk : string.Concat(searchDisk, ":");
                searchDisk = searchDisk.EndsWith("\\") ? searchDisk : searchDisk + "\\";
            }
            while (!System.IO.Directory.Exists(searchDisk));
            //string[] drives = System.IO.Directory.GetLogicalDrives();

            return searchDisk;
        }

        static void SetConsoleTitle(object sender, SetConsoleTitleEventArgs e)
        {
            Console.Title = e.Title;
        }

        static void PrintCancelPressed(object sender, EventArgs e)
        {
            Console.WriteLine("Cancellation of execution...");
        }
    }
}

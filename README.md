# FileSearchMT

***

## Name
FileSearchMT

## Description
File Search Multi-threaded console application. Works many times faster than built-in OS tools. Especially with SSD drives.

## Requirements
Windows OS, with .NET Framework 4.6.2 or greater.

## Authors
Eugene A. Grebenyuk

## License
FileSearchMT is Freeware

## Project status
Beta version. Almost completed.
